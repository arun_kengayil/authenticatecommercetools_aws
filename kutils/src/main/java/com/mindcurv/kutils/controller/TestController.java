package com.mindcurv.kutils.controller;


import com.commercetools.api.client.ProjectApiRoot;
import com.commercetools.api.models.project.Project;
import com.mindcurv.kutils.config.AwsSecretConfig;
import com.mindcurv.kutils.service.authService.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import static com.mindcurv.kutils.utils.FileUtils.*;
import static com.mindcurv.kutils.config.CommerceToolsAPIConfig.*;

@RestController
@RequestMapping(value = "/ken")
public class TestController {

    @Value("${file.read.path}")
    String filePath;

    @Autowired
    AuthService authService;

    @GetMapping
    public ResponseEntity<String> welcome(){
        return new ResponseEntity<>("Greetings User", HttpStatus.OK);
    }

    @GetMapping(value="/readText")
    public ResponseEntity<String> readText() throws IOException {
        return new ResponseEntity<>(readTextFile(filePath).toString(), HttpStatus.OK);
    }

    @GetMapping(value="/authenticateCommerceTools")
    public ResponseEntity<Map<String,Object>> authenticateCommerceTools() throws IOException {
        return new ResponseEntity<>(authService.authenticateCommerceTools(), HttpStatus.OK);
    }
}
