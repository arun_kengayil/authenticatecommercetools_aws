package com.mindcurv.kutils.controller;

import com.mindcurv.kutils.dto.CustomerDetails;
import com.mindcurv.kutils.service.customerService.CustomerService;
import com.mindcurv.kutils.service.productService.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping(value = "/products")
public class ProductController {

    @Autowired
    ProductService productService;

    @PostMapping(value = "/getProductTypes", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Map> createCustomer(@RequestBody Map<String, Object> parameterMap) throws IOException {
        int offset = (Integer) parameterMap.get("offset");
        int limit = (Integer) parameterMap.get("limit");
        return new ResponseEntity(productService.getProductTypes(offset, limit), HttpStatus.OK);
    }
}
