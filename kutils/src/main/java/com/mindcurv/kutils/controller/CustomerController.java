package com.mindcurv.kutils.controller;

import com.mindcurv.kutils.dto.CustomerDetails;
import com.mindcurv.kutils.service.customerService.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.util.Map;


@RestController
@RequestMapping(value = "/customer")
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @PostMapping(value = "/createCustomer", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Map> createCustomer(@RequestBody CustomerDetails customerDetails) throws IOException {
        return new ResponseEntity(customerService.createCustomer(customerDetails), HttpStatus.OK);
    }

    @PostMapping(value = "/login")
    public ResponseEntity<Map> customerLogin(@RequestBody Map<String, Object> parameterMap) throws IOException {
        String email = (String) parameterMap.get("email");
        String password = (String) parameterMap.get("password");
        return new ResponseEntity(customerService.customerLogin(email, password), HttpStatus.OK);
    }

    @PostMapping(value = "/getEmailVerifyToken", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Map> getEmailVerifyToken(@RequestBody Map<String, Object> parameterMap) throws IOException {
        String id = (String) parameterMap.get("customerId");
        Long ttlMinutes = ((Integer) parameterMap.get("tokenValidityMinutes")).longValue();
        return new ResponseEntity(customerService.getEmailVerifyToken(id, ttlMinutes), HttpStatus.OK);
    }

    @PostMapping(value = "/verify", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Map> customerVerification(@RequestBody Map<String, Object> parameterMap) throws IOException {
        String token = (String) parameterMap.get("emailVerifyToken");
        return new ResponseEntity(customerService.verifyEmailVerifyToken(token), HttpStatus.OK);
    }

}
