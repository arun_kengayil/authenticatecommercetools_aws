package com.mindcurv.kutils.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class CustomerDetails {

    private String customerNumber;
    @NotNull
    private String companyName;
    @NotNull
    private String email;
    @NotNull
    private String password;
    private String firstName;
    private String lastName;
    private String middleName;
    private String title;

    private LocalDate dateOfBirth;
    private String customerKey;
}
