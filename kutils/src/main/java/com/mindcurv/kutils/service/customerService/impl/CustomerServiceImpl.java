package com.mindcurv.kutils.service.customerService.impl;

import com.commercetools.api.client.*;
import com.commercetools.api.models.customer.*;
import com.mindcurv.kutils.dto.CustomerDetails;
import com.mindcurv.kutils.service.customerService.CustomerService;
import io.vrap.rmf.base.client.ApiHttpResponse;
import static org.apache.commons.lang3.StringUtils.*;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.Optional;
import static com.mindcurv.kutils.config.CommerceToolsAPIConfig.getApiRoot;

@Service
public class CustomerServiceImpl implements CustomerService {

    private static final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

    public CustomerDraftBuilder createCustomerDraftBuilder(@NotNull CustomerDetails customerDetails){
        CustomerDraftBuilder customerBuilder = CustomerDraft.builder();
        customerBuilder.companyName(customerDetails.getCompanyName());
        customerBuilder.email(customerDetails.getEmail());
        customerBuilder.password(customerDetails.getPassword()).build();
        customerBuilder.firstName(customerDetails.getFirstName());
        customerBuilder.middleName(customerDetails.getMiddleName());
        customerBuilder.lastName(customerDetails.getLastName());
        customerBuilder.dateOfBirth(customerDetails.getDateOfBirth());
        customerBuilder.key(customerDetails.getCustomerKey());
        customerBuilder.customerNumber(customerDetails.getCustomerNumber());
        return customerBuilder;
    }
    @Override
    public Optional<Customer> createCustomer(@NotNull CustomerDetails customerDetails) throws IOException {
        if(isAnyEmpty(customerDetails.getEmail(),
                customerDetails.getPassword(),
                customerDetails.getCompanyName())){
            return Optional.empty();
        }
        CustomerDraftBuilder customerDraftBuilder = createCustomerDraftBuilder(customerDetails);
        CustomerDraft customerDraft = customerDraftBuilder.build();
        ByProjectKeyCustomersPost post = getApiRoot().customers().post(customerDraft);
        ApiHttpResponse<CustomerSignInResult> result = post.executeBlocking();
        Customer customer = result.getBody().getCustomer();
        return Optional.of(customer);
    }

    @Override
    public Optional<CustomerSignInResult> customerLogin(@NotNull String email,
                                                        @NotNull String password) throws IOException {
        if(isAnyEmpty(email, password)){
            return Optional.empty();
        }
        CustomerSigninBuilder customerSigninBuilder = CustomerSignin.builder();
        customerSigninBuilder.email(email);
        customerSigninBuilder.password(password);
        ByProjectKeyLoginPost post = getApiRoot().login()
                .post(customerSigninBuilder.build());
        ApiHttpResponse<CustomerSignInResult> result = post.executeBlocking();
        return Optional.of(result.getBody());
    }

    @Override
    public Optional<CustomerToken> getEmailVerifyToken(@NotNull String id,
                                                       @NotNull Long ttlMinutes) throws IOException {
        if(isEmpty(id) || ttlMinutes <= 0){
            return Optional.empty();
        }
        CustomerCreateEmailTokenBuilder customerCreateEmailTokenBuilder = CustomerCreateEmailToken.builder();
        customerCreateEmailTokenBuilder.id(id);
        customerCreateEmailTokenBuilder.ttlMinutes(ttlMinutes);
        CustomerCreateEmailToken customerCreateEmailToken = customerCreateEmailTokenBuilder.build();
        ByProjectKeyCustomersEmailTokenRequestBuilder tokenRequestBuilder = getApiRoot().customers().emailToken();
        ByProjectKeyCustomersEmailTokenPost emailTokenPost = tokenRequestBuilder.post(customerCreateEmailToken);
        ApiHttpResponse<CustomerToken> response = emailTokenPost.executeBlocking();
        return Optional.of(response.getBody());
    }

    @Override
    public Optional<Customer> verifyEmailVerifyToken(@NotNull String emailVerifyToken) throws IOException {
        if(isEmpty(emailVerifyToken)){
            return Optional.empty();
        }
        CustomerEmailVerifyBuilder customerEmailVerifyBuilder = CustomerEmailVerify.builder();
        CustomerEmailVerify customerEmailVerify = customerEmailVerifyBuilder
                .tokenValue(emailVerifyToken).build();
        ByProjectKeyCustomersEmailConfirmPost post = getApiRoot().customers()
                .emailConfirm().post(customerEmailVerify);
        ApiHttpResponse<Customer> result = post.executeBlocking();
        return Optional.of(result.getBody());
    }
}
