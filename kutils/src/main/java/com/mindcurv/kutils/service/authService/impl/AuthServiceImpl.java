package com.mindcurv.kutils.service.authService.impl;

import static com.mindcurv.kutils.config.CommerceToolsAPIConfig.*;

import com.commercetools.api.client.ProjectApiRoot;
import com.commercetools.api.models.project.Project;
import com.mindcurv.kutils.service.authService.AuthService;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class AuthServiceImpl implements AuthService {

    @Override
    public Map<String, Object> authenticateCommerceTools() throws IOException {
        Map<String,Object> map = new HashMap<>();
        ProjectApiRoot apiRoot = getApiRoot();
        Project response = apiRoot
                .get()
                .executeBlocking().getBody();
        map.put("success", true);
        map.put("countries", response.getCountries());
        map.put("key", response.getKey());
        return map;
    }
}
