package com.mindcurv.kutils.service.productService;

import com.commercetools.api.client.ByProjectKeyProductTypesGet;
import com.commercetools.api.models.product_type.ProductType;
import com.commercetools.api.models.product_type.ProductTypePagedQueryResponse;
import io.vrap.rmf.base.client.ApiHttpResponse;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public interface ProductService {

    Optional<List<ProductType>> getProductTypes(int offset, int limit) throws IOException;
}
