package com.mindcurv.kutils.service.customerService;

import com.commercetools.api.models.customer.*;
import com.mindcurv.kutils.dto.CustomerDetails;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

public interface CustomerService {

    public Optional<Customer> createCustomer(CustomerDetails customerDetails) throws IOException;

    public Optional<CustomerSignInResult> customerLogin(String email, String password) throws IOException;

    public Optional<CustomerToken> getEmailVerifyToken(String id, Long ttlMinutes) throws IOException;

    public Optional<Customer> verifyEmailVerifyToken(String emailVerifyToken) throws IOException;
}
