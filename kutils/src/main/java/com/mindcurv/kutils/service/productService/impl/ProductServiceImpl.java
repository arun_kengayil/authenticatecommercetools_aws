package com.mindcurv.kutils.service.productService.impl;

import com.commercetools.api.client.ByProjectKeyProductTypesGet;
import com.commercetools.api.client.ByProjectKeyProductTypesRequestBuilder;
import com.commercetools.api.client.ByProjectKeyProductsPost;
import com.commercetools.api.models.common.LocalizedString;
import com.commercetools.api.models.common.LocalizedStringBuilder;
import com.commercetools.api.models.product.Product;
import com.commercetools.api.models.product.ProductDraft;
import com.commercetools.api.models.product.ProductDraftBuilder;
import com.commercetools.api.models.product_type.*;
import com.mindcurv.kutils.config.CommerceToolsAPIConfig;
import com.mindcurv.kutils.service.productService.ProductService;
import io.vrap.rmf.base.client.ApiHttpResponse;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import static org.apache.commons.lang3.StringUtils.*;

import static com.mindcurv.kutils.config.CommerceToolsAPIConfig.getApiRoot;

@Service
public class ProductServiceImpl implements ProductService {

    @Override
    public Optional<List<ProductType>> getProductTypes(int offset, int limit) throws IOException {
        if(offset < 0 || limit < 0){
            return Optional.empty();
        }
        ByProjectKeyProductTypesRequestBuilder requestBuilder = CommerceToolsAPIConfig.getApiRoot().productTypes();
        ByProjectKeyProductTypesGet byProjectKeyProductTypesGet  = requestBuilder.get();
        if(offset > 0){
            byProjectKeyProductTypesGet.addOffset(offset);
        }
        if(limit >0){
            byProjectKeyProductTypesGet.addLimit(limit);
        }
        /*if(false){
            byProjectKeyProductTypesGet.addWhere("");
        }*/
        ProductTypePagedQueryResponse response = byProjectKeyProductTypesGet.executeBlocking().getBody();
        return Optional.of(response.getResults());
    }

    public Product addProduct() throws IOException {
        ProductDraftBuilder productDraftBuilder = ProductDraft.builder();
        ProductDraft productDraft = productDraftBuilder.build();

        LocalizedStringBuilder localizedStringBuilder = LocalizedString.builder();
        LocalizedString localizedString = localizedStringBuilder.build();
        localizedString.setValue("", "");
        //localizedString.setValue("", "");
        //localizedString.setValue("", "");
        ProductTypeResourceIdentifierBuilder productTypeResourceIdentifierBuilder = ProductTypeResourceIdentifier.builder();
        ProductTypeResourceIdentifier productTypeResourceIdentifier = productTypeResourceIdentifierBuilder.build();
        productDraft.setName(localizedString);
        productDraft.setProductType(productTypeResourceIdentifier);

        ByProjectKeyProductsPost byProjectKeyProductsPost = getApiRoot().products().post(productDraft);
        ApiHttpResponse<Product> response =  byProjectKeyProductsPost.executeBlocking();
        return response.getBody();
    }

    public void addProductTypes(){
        ProductTypeBuilder productTypeBuilder = ProductType.builder();
        ProductType productType = productTypeBuilder.build();
    }
}
