package com.mindcurv.kutils.service.authService;

import java.io.IOException;
import java.util.Map;

public interface AuthService {


    public Map<String, Object> authenticateCommerceTools() throws IOException;
}
