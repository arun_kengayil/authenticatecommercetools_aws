package com.mindcurv.kutils.config;

import com.commercetools.api.client.ProjectApiRoot;
import com.commercetools.api.defaultconfig.ApiRootBuilder;
import com.commercetools.api.defaultconfig.ServiceRegion;
import com.commercetools.importapi.defaultconfig.ImportApiRootBuilder;
import com.commercetools.importapi.models.importcontainers.ImportContainer;
import com.commercetools.importapi.models.importcontainers.ImportContainerBuilder;
import com.google.gson.JsonObject;
import static com.mindcurv.kutils.KutilsApplication.*;

import io.vrap.rmf.base.client.ServiceRegionConfig;
import io.vrap.rmf.base.client.oauth2.ClientCredentials;

import java.io.IOException;
import java.util.Properties;

public class CommerceToolsAPIConfig {

    private static ProjectApiRoot apiRoot;

    private CommerceToolsAPIConfig(){}

    public static ProjectApiRoot getApiRoot() throws IOException {
        //commenting reading from properties file
        /*final Properties prop = new Properties();
        prop.load(CommerceToolsAPIConfig.class.getResourceAsStream("/commercetools.properties"));
        String projectKey = prop.getProperty("projectKey");
        String clientId = prop.getProperty("clientId");
        String clientSecret = prop.getProperty("clientSecret");*/
        if(apiRoot == null){
            String projectKey = awsSecret.get("projectKey").getAsString();
            String clientId = awsSecret.get("clientId").getAsString();
            String clientSecret = awsSecret.get("clientSecret").getAsString();
            apiRoot = ApiRootBuilder.of().defaultClient(
                    ClientCredentials.of().withClientId(clientId)
                            .withClientSecret(clientSecret)
                            .build(),
                    ServiceRegion.GCP_EUROPE_WEST1).build(projectKey);
        }
        return apiRoot;
    }

    public static ImportApiRootBuilder getImportRoot(){
        return null;
    }
}
