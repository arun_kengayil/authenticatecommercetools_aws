package com.mindcurv.kutils;

import com.google.gson.JsonObject;
import com.mindcurv.kutils.config.AwsSecretConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import static org.apache.commons.lang3.StringUtils.*;
import java.util.List;


@SpringBootApplication
public class KutilsApplication implements ApplicationRunner {

	public static JsonObject awsSecret;

	private static final Logger log = LoggerFactory.getLogger(KutilsApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(KutilsApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		final List<String> argsList = args.getNonOptionArgs();
		if(!argsList.isEmpty()){
			final String aws_secret_arn = argsList.get(0);
			final String aws_region = argsList.get(1);
			if(!isAnyEmpty(aws_secret_arn, aws_region)){
				awsSecret =  AwsSecretConfig.getSecret(aws_secret_arn, aws_region);
			}else{
				log.error("AWS Command line arguments missing...");
			}
		}
	}
}
