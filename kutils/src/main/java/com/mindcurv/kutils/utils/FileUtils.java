package com.mindcurv.kutils.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileUtils {

    public static List<String> readTextFile(String pathToFile) throws IOException {
        return Files.readAllLines(Paths.get(pathToFile));
    }
}
